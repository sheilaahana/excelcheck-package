<head>
    <title>Test2 - Sheila Hana A</title>
    <style>
        body {
            text-align:center; 
            margin-top: 50px;
        }
        label {
            margin : 20px 50px
        }
        input, select { 
            margin : 10px;
            width : 200px;
            height : 35px;
        }
        table, td {
            border : 1px black solid;
            margin : auto;
        }
        td{
            padding:10px
        }
    </style>
</head>
<body>
    <h1>Result</h1>   
    <h3> {{$data['message']}} </h3>

    @if(isset($data['code']) &&$data['code'] == 'ERROR_DATA')
        <table>
            <thead>
                <tr>
                    <td>Row</td>
                    <td>Error</td>
                </tr>
            </thead>    
            <tbody>
                @foreach($data['data'] as $key => $error)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{!! implode('<br>' , $error ) !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>        
    @endif
    <br>
    <a href="{{ url('demo-view') }}" >Check Again</a>
</body>