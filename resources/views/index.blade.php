<head>
    <title>Test2 - Sheila Hana A</title>
    <style>
        body {
            text-align:center; 
            margin-top: 50px;
        }
        label {
            margin : 20px 50px
        }
        input, select { 
            margin : 10px;
            width : 200px;
            height : 35px;
        }
    </style>
</head>
<body>
    <h1>Excel Format Value</h1>   
    <form action="{{ url('demo-view') }}" method="post" enctype='multipart/form-data'>
        <label>Input Excel :</label><br>
        <input type="file" name="file" required><br>
        Type : <br>
        <i>( this type get from config )</i><br>
        <select name="type" id="">
            @foreach($type as $key => $file)
            <option value="{{$key}}">{{ $key }}</option>
            @endforeach
        </select>
        <br>
        <button type="submit">Check Excel</button>
    </form>
</body>