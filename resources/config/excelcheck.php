<?php 

return [
    'type' => [
        //type => Aliases Helper
        'type-A' => 'TypeA',
        'type-B' => 'TypeB',
        'type-C' => 'TypeC',
    ],

    'aliases' => [
        'Excel' => sheilaahana\excelcheck\Helpers\Excel::class,
        'TypeA' => sheilaahana\excelcheck\Helpers\TypeA::class,
        'TypeB' => sheilaahana\excelcheck\Helpers\TypeB::class,
        'TypeC' => sheilaahana\excelcheck\Helpers\TypeC::class,
    ]
];