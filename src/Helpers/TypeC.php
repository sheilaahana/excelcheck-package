<?php

class TypeC {

    static function checkColumn($column){
        $rules = [
            '#Field_A*','#Field_B'
        ];

        // custom rule column name can be different, but same column count

        $diff = count(array_filter($column)) - count($rules);
        if($diff !== 0){
            return "Differenct Column Count";
        }else{
            return true;
        }
    }
    static function checkData($datas){
        $row = 2;
        foreach($datas as $values){
            foreach($values as $column => $value){
                if(substr($column, -1) == '*'){
                    if($value == null){
                        $output[$row][] = "Missing value in ".$column;
                    }
                }
                if(substr($column, 0, 1) == '#'){
                    // custom rule # column cannot be filled with special character 
                    if(preg_match("/[^a-zA-Z0-9\']/",$value) > 0){
                        $output[$row][] = $column." should not contain any special character";
                    }
                }
            }
            $row++;
        }
        if(isset($output)){
            return $output;
        }

        return true;
    }
}