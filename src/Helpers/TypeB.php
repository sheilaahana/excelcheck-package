<?php

class TypeB {

    static function checkColumn($column){
        $rules = [
            'Field_A*','#Field_B'
        ];

        $result = Excel::checkColumn($column, $rules);

        return $result;
    }
    static function checkData($datas){
        $result = Excel::checkData($datas);

        return $result;
    }
}