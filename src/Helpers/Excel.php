<?php

class Excel {
    static function checkColumn($column,$rules)
    {
        $diff = array_diff(array_filter($column),$rules);
        if(!empty($diff)){
            $diff = implode(' ',$diff);
            if(trim($diff,' ') == ''){
                return "Some column is missing";
            }
            return $diff;
        }else{
            return true;
        }
    }
    static function checkData($datas){
        $row = 2;
        foreach($datas as $values){
            foreach($values as $column => $value){
                if(substr($column, -1) == '*'){
                    if($value == null){
                        $output[$row][] = "Missing value in ".$column;
                    }
                }
                if(substr($column, 0, 1) == '#'){
                    if(substr_count($value, ' ') > 0){
                        $output[$row][] = $column." should not contain any space";
                    }
                }
            }
            $row++;
        }
        if(isset($output)){
            return $output;
        }

        return true;
    }

    static function getValue($file, $dataType = 'data')
    {
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(TRUE);

        $spreadsheet = $reader->load($file);
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        $highestColumn = $worksheet->getHighestColumn();

        $column = [];
        for($col='A'; $col<=$highestColumn; $col++){
            $column[] =  $worksheet->getCell($col . 1)->getValue();
        }

        if($dataType == 'column'){
            return $column;
        }
        
        $data = [];
        $value = [];
        for($row=2; $row<=$highestRow; $row++){
            $dataRow = [];
            $i = 0;
            for($col='A'; $col<=$highestColumn; $col++){
                $dataRow += array($column[$i] => $worksheet->getCell($col . $row)->getValue());
                $i++;
            }
            $data[] = $dataRow;
        }

        if($dataType == 'data'){
            return $data;
        }
    }
}
