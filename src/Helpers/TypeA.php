<?php

class TypeA {

    static function checkColumn($column){
        $rules = [
            'Field_A*','#Field_B','Field_C','Field_D*','Field_E*'
        ];

        $result = Excel::checkColumn($column, $rules);

        return $result;
    }
    static function checkData($datas){
        $result = Excel::checkData($datas);

        return $result;
    }
}