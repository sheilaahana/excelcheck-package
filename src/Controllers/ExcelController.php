<?php

namespace sheilaahana\excelcheck\Controllers;

use Illuminate\Http\Request;
use Excel;

class ExcelController extends Controller
{
    public function index(){
        $types = config('excelcheck.type');
        return view('excelcheck::index')->with(['type' => $types]);
    }
    public function process(Request $request){
        $file = $request->file('file');

        $excel = new Excel;
        $column = $excel->getValue($file, 'column');
        $data = $excel->getValue($file, 'data');

        $function = config('excelcheck.type.'.$request->type);

        $output['message'] = 'Data Clear';
        // start checking 
        $checkColumn = $function::checkColumn($column);
        if($checkColumn !== true ){
            $output = [
                'code' => 'ERROR_COLUMN',
                'message' => "Invalid columm ( ". $checkColumn . " )",
            ];
        }else{
            // will not checking data if column invalid
            $checkData = $function::checkData($data);
            if($checkData !== true ){
                $output = [
                    'code' => 'ERROR_DATA',
                    'message' => "Invalid data value",
                    'data' => $checkData
                ];
            }
        }

        return view('excelcheck::result')->with(['data' => $output]);
    }
}
