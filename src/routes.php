<?php

Route::get('demo', function() {
    echo 'Hello from the demo package!';
});

Route::get('demo-view', 'sheilaahana\excelcheck\Controllers\ExcelController@index');
Route::post('demo-view', 'sheilaahana\excelcheck\Controllers\ExcelController@process');