<?php

namespace sheilaahana\excelcheck\Providers;

use Illuminate\Support\ServiceProvider;

class ExcelCheckServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__.'/../../resources/config/excelcheck.php' => config_path('excelcheck.php'),
        ]);

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'excelcheck');

        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename){
            require_once($filename);
        }

        // $value = config('excelcheck.option');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes.php');
    }
}
