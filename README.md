# ExcelCheck Package

Repo ini di buat untuk memenuhi kebutuhan assignment test dari Avana.id

Sedikit penjelasan untuk package ini.
ExcelCheck ini berfungsi untuk memvalidasi file excel yang di berikan dengan rule yang di tentukan :
- Kolom yang harus sesuai dengan rule yang di validasi ( nama & jumlah )
- kolom yang menentukan rule value datanya ( * required, # tidak boleh spasi )
- ( tambahan ) satu kolom bisa memiliki kedua rule ( * # )

improvisasi untuk point rule file,
apabila kedepannya ternyata ada rule tambahan, hanya perlu menambahkan value baru di 
config.excelcheck.type 
dengan format (typeFile => NamaAliases Helper )
// 'type-C' => 'TypeC'
dan 
config.excelcheck.aliases
'TypeC' => sheilaahana\excelcheck\Helpers\TypeC::class,

isian typeFile akan digunakan di form option untuk memilih type file apa yang di upload, 
nama aliases Helper, agar secara dinamis memanggil helper yang sudah di require (file ExcelCheckServiceProvider) tanpa harus menambahkan baris "use" di controller.

file TypeC.php / file rule tambahan di simpan di folder Helpers

sebagai pembanding, untuk typeA dan typeB memanggil fungsi yang sama sehingga menggunakan fungsi general di helper Excel.php,
sedangkan TypeC memiliki custom Rule sendiri, yaitu :
- nama kolom boleh berbeda, tapi jumlah kolom harus sama
- kolom dengan awalan # tidak boleh diisi dengan special character

saya lampirkan video penggunaaan aplikasi.

https://drive.google.com/drive/folders/1SRlAQVMT5Rp50gm_zHmMvOX62lPVUV25?usp=sharing

dengan reproduce yang akan di lakukan di video :
1. Type A - invalid data
2. Type A - valid data
3. File Type A with Type B Option 
4. File B - invalid data
5. File B - valid data
6. File C - invalid data
7. File C - invalid column
8. File C - valid data
